import React, {Component} from 'react'
import {
    View,
    Text
} from 'react-native'
import { Dropdown } from 'react-native-material-dropdown';
import {Button} from 'react-native-elements'
import Styles from '../../assets/styles/ChooseScreenStyle'
import {getAllCategories} from '../../libs/GetCategories'


export default class ChooseScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: '',
            idCate: '',
            isloaded: false
        }
        this.data = [];
    }
    onValueInputChange(value, index, data) {
        this.setState({text: value, idCate: data[index]});
    }

    onPressButton() {

        this.props.navigation.pop();
    }

    componentDidMount() {
        getAllCategories().then( (list) => {
            this.data = list.map((list) => {
                return {value: list.name}
            })
            this.setState({isloaded: true})
        })
    }
    render() {
        const {data} = this;
        return (
            <View style = {Styles.container}>
                <View style = {Styles.marginView}>
                    <Text style={[Styles.text, Styles.marginElement]}>What's your interest??</Text>
                    <Dropdown
                        label='Choose category'
                        data={data}
                        onChangeText = {this.onValueInputChange.bind(this)}
                        containerStyle = {Styles.dropDownContainer}
                        itemTextStyle = {Styles.item}
                    />
                    <Button 
                        style = {[Styles.marginElement, Styles.fixButton]}
                        title = {`Apply ${this.state.text} now`}
                        type = 'outline'
                        onPress = {this.onPressButton.bind(this)}
                    />
                </View>
              
            </View>
        )
    }
}

