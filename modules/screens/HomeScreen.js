import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  FlatList,
} from 'react-native';

import HomeScreenStyle from '../../assets/styles/HomeStyle';
import CardItemView from '../views/CardItemView';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationHomeView } from '../views/NavigationTopview';
import DetailScreen from '../screens/DetailScreen';
import GetProducts from '../../libs/GetProducts'
import Product from '../../model/Product'
import codePush from "react-native-code-push";
import { connect } from 'react-redux';

export class HomeScreen extends Component {

  constructor(props) {
    super()
    this.state = {
      isLoading: true,
      products: []
    };
    this.listProduct = [];
  }
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  componentDidMount() {
    console.log("componentDidMount ")
    GetProducts.getListProducts()
      .then(data => {
        // Reload List
        this.setState({ products: data })
        setTimeout(() => {
          this.setState({ isLoading: false });
        }, 2000)

      })
      .catch(error => {
        console.error(error)
      });

  }
  componentDidUpdate() {
    console.log("componentDidUpdate")
  }
  _onClickProductItem = (product) => {
    this.props.navigation.push('Detail', { product: product });
  }
  renderProduct() {
    listRenderProduct = this.listProduct.map((product, index) => {
      return (<CardItemView
        action={() => {
          this._onClickProductItem(product)
        }}
        image={product.image}
        title={product.name}
        price={product.price}
        key={`${product.id}_${index}`}
      />)

    });

    return listRenderProduct;

  }
  generateHash = () => {
    return Math.random().toString(36).substring(7);
  }
  trigger() {
    alert('YEAH')
  }

  renderFooterView() {
    return (
      <View style={[HomeScreenStyle.footer]}>
        <ActivityIndicator size='large' />
      </View>
    )
  }
  renderBanner() {
    return (
      <View style = {HomeScreenStyle.bannerContainer}>
          
      </View>
    )
  }
  renderFlatListProduct = () => {

    return <FlatList
      data={this.state.products}
      style={HomeScreenStyle.flatListStyle}
      renderItem={({ item }, index) => {
        switch (index % 10) {
          case 0: {
            return (
              <CardItemView
                action={() => {
                  this._onClickProductItem(item)
                }}
                image={item.image}
                title={item.name}
                price={item.price}
              />)
          }
          default: {
            return (
              <CardItemView
                action={() => {
                  this._onClickProductItem(item)
                }}
                image={item.image}
                title={item.name}
                price={item.price}
              />)
          }
        }
      }}
      keyExtractor={(item, index) => `${item.id}_${index}`}
      numColumns={2}
      onEndReached={this.reloadNewData}
      onEndReachedThreshold={0.5}
      ListFooterComponent={this.renderFooterView()}
      removeClippedSubviews={true}
      initialNumToRender={10}
      maxToRenderPerBatch={15}
      extraData={this.state}
    />

  }

  onButtonPress() {
    this.props.navigation.push('Choose');
  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  reloadNewData = () => {
    console.log("call me agains??")

    const max = 50;
    let products = this.state.products;

    this.state.products.forEach((product, index) => {
      if (index < max) {
        products.push(product)
      }
    })
    this.setState({ products })


  }
  handelScroll = ({ nativeEvent }) => {
    if (this.isCloseToBottom(nativeEvent)) {
      this.reloadNewData()
    }
  }

  render() {

    const { isLoading, isAddingProduct } = this.state;
    const products = this.listProduct;
    return [
      <StatusBar backgroundColor="#ff4d82" barStyle="dark-content" />,
      <NavigationHomeView
        pressButton={this.onButtonPress.bind(this)}
      />,
      { ...this.renderFlatListProduct() }
    ];
  }
}


