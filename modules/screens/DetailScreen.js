import React, {Component} from 'react';
import Tags from "react-native-tags";
import {View, 
        Text,
        Button, 
        ScrollView,
        Image,
} 
        
from 'react-native';
import Icon from 'react-native-ionicons'

import {ProductDetailStyle} from './../../assets/styles/ProductDetailStyle';
const TabIcon = (props) => (
    <Icon ios="apps" android="apps" />
);
  
export default class DetailScreen extends Component {
    static navigationOptions = {
        tabBarIcon: TabIcon,
        headerStyle: {
            backgroundColor: '#ff4d82',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
        },
        height: 60
    };
    setImage(image) {
        if (image == null) {
            
            return require('../../assets/images/placeholder.jpg');
        } else {
            return {uri: image};
        }
    }

    render () {
        const {product} = this.props.navigation.state.params;
        console.log(product);
        return (
        
        <ScrollView>
            <View style = {ProductDetailStyle.container}>
            
                <View style = {ProductDetailStyle.productInfo}>
                    <Image style= {ProductDetailStyle.productInfoImage}
                            source= {this.setImage(product.image)}
                    />
                    <View style = {ProductDetailStyle.productInfoTextBlock}>
                        <View>
                            <Text style={[
                                ProductDetailStyle.productInfoTextTintColor,
                                ProductDetailStyle.productInfoMarginItem
                                ]}
                            >{product.categoryName}</Text>

                        </View>
                        <Text style= {{fontWeight: 'bold'}}>{product.name}</Text>
                        <Text style={[
                                ProductDetailStyle.productInfoTextTintColor,
                                ProductDetailStyle.productInfoMarginItem
                                ]}
                        >{product.price}</Text>
                    </View> 
                </View> 
                                

                <View style= {ProductDetailStyle.productStatus}>
                    <Text 
                    style = {
                        [ProductDetailStyle.productInfoDisableText],
                        ProductDetailStyle.textMargin,
                        {marginLeft: 10, marginBottom: 10}
                        }>Tình trạng sản phẩm</Text>

                    <Text style={{
                        marginLeft: 10
                    }}>Mới 100%</Text>
                </View>

                <View style={ProductDetailStyle.tag}>
                    <Text style={{paddingLeft: 10}}>Hashtags</Text>
                    <View style={ProductDetailStyle.tagContent}
                    >
                        {Tag('AnMac')}
                        {Tag('Uong')}
                        {Tag('AnMac')}
                        {Tag('Uong')}
                        {Tag('AnMac')}
                        {Tag('Uong')}
                        
                    </View>
                </View>

                <View style={ProductDetailStyle.tag}>
                    <Text style={{paddingLeft: 10}}>#Mô tả sản phẩm</Text>
                    <View style={ProductDetailStyle.tagContent}
                    >
                        <Text style={ProductDetailStyle.textOfTag}> Đồng hồ của sơn tùng MTP</Text>
                        
                    </View>
                </View>
                
                <View style = {ProductDetailStyle.tag}>
                    <Text style={{paddingLeft: 10}}>Người đăng tin</Text>

                    <View style={ProductDetailStyle.topView}>
                        <Image
                            style = {ProductDetailStyle.avata}
                            source = {this.setImage(product.postAuthorImage)}
                        />
                        <Text style={ProductDetailStyle.textBlock}>{product.postAuthorName}</Text>
                    </View>
                </View>

                <View style={ProductDetailStyle.reviewBlockParent} >
                    <View style={ProductDetailStyle.reviewBlockChild}>

                        {reviewBlock('Đánh giá', 0, 0)}
                        {reviewBlock('Đang bán', 2, 1)}
                        {reviewBlock('Đã bán', 3, 2)}

                    </View>
                    

                </View>

            </View>

            <Button title = "Báo cáo vi phạm"
                    style = {ProductDetailStyle.button}
            ></Button>

            

        </ScrollView>
            
        );
    }
}

const Tag = (text) => {
    return (
        <View style = {ProductDetailStyle.tagView}>
            <Text style={ProductDetailStyle.textOfTag}>{text}</Text>
        </View>
    )
}
const reviewBlock = (title, amount, index) => {
    let line = index == 1 ? ProductDetailStyle.line : ProductDetailStyle.noneLine;
    return (
        <View style={ProductDetailStyle.reviewBlock}>
            <View style = {line}>
                    <Text style={{marginLeft: 10}}>{title}</Text>
                    <Text style={{marginLeft: 10, marginTop: 10}}>{amount}</Text>
            </View>
        </View>
    )
}