import React, {Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {HomeNavigationStyle} from '../../assets/styles/NavigationStyle'
import { SearchBar, Button as RcButton } from 'react-native-elements';
import { Button } from 'native-base';

export class NavigationHomeView extends Component {
    state = {
        search: '',
    };
    updateSearch = search => {
        this.setState({ search });
    };
    render() {

        return (
            <View style = {HomeNavigationStyle.container}>
                <View style= {HomeNavigationStyle.topView}>
                    <Text style= {HomeNavigationStyle.text}>3KM</Text>
                    <View style = {HomeNavigationStyle.searchView}>
                        <SearchBar
                            lightTheme = 'default'
                            placeholder="Bạn tìm gì hôm nay?"
                            onChangeText={this.updateSearch}
                            value={this.state.search}
                            inputContainerStyle = {HomeNavigationStyle.searchInputContainer}
                            containerStyle = {HomeNavigationStyle.inputStyle}
                            inputStyle =  {HomeNavigationStyle.textSearchInput}
                        />
                    </View>
                </View>
                <TouchableOpacity>
                    <View style = {HomeNavigationStyle.categoriesButton}>
                    <RcButton
                        title="Choose Category"
                        buttonStyle= {HomeNavigationStyle.button}
                        onPress = {this.props.pressButton}
                        
                    />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}