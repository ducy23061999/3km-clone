import React, { Component } from 'react'
import {View, Text} from 'react-native'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {HomeScreen} from '../screens/HomeScreen'
import DetailScreen from '../screens/DetailScreen';
import Icon from 'react-native-ionicons'
import DemoScreen from '../screens/DemoScreen'
import ChooseScreen from '../screens/ChooseScreen'



const MainNavigator = createStackNavigator({
  HomeStack: {screen: HomeScreen},
  Detail: {screen: DetailScreen},
  Choose: {screen: ChooseScreen}
});

const TabIcon = (props) => (
  <Icon ios="home" android="home" />

)


const App = createAppContainer(MainNavigator);

const BottomTabNavigator = createBottomTabNavigator({
    Home: {
      screen: App,
      navigationOptions: {
        tabBarIcon: TabIcon,
        
      }
    },
    Explore: {
      screen: DemoScreen,
      navigationOptions: {
        tabBarIcon: <Icon ios="apps" android="apps" />
      }
    },
    Create: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: <Icon ios="person" android="person" />
      }
    }
  }, {
    tabBarOptions: {
      showLabel: false,
      showIcon: true
    }
  });
  
export default BottomTabNavigator;

