import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tabbar from 'react-native-tabbar-bottom'
import {HomeScreen} from '../screens/HomeScreen'

export class TabbarView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "HomeScreen"
    }
  };

  render() {
    return (
        <View style={styles.container}>
        {
          // if you are using react-navigation just pass the navigation object in your components like this:
          // {this.state.page === "HomeScreen" && <MyComp navigation={this.props.navigation}>Screen1</MyComp>}
          
        }
        {this.state.page === "HomeScreen"  }
        {this.state.page === "NotificationScreen"}
        {this.state.page === "ProfileScreen"}
        {this.state.page === "ChatScreen"}
        {this.state.page === "SearchScreen"}
            <Tabbar
                stateFunc={(tab) => {
                  this.setState({page: tab.page})
                  console.log(tab);
                //this.props.navigation.setParams({tabTitle: tab.title})
                }}
                activePage={this.state.page}
                tabs={[
                    {
                        page: "HomeScreen",
                        icon: "home",
                    },
                    {
                        page: "NotificationScreen",
                        icon: "notifications",
                        badgeNumber: 11,
                    },
                    {
                        page: "ProfileScreen",
                        icon: "person",
                    },
                    {
                        page: "ChatScreen",
                        icon: "chatbubbles",
                        badgeNumber: 7,
                    },
                    {
                        page: "SearchScreen",
                        icon: "search",
                    },
                ]}
            />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 400
  }
});
