import React,{ PureComponent } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import HomeScreenStyle from '../../assets/styles/HomeStyle';
import ProductDetailScreen from "../screens/ProductDetailScreen";
import {
    Image
} from 'react-native-elements'

export default class CardItemView extends PureComponent {

    constructor(props) {
        super(props);
    }
    
    setImage(image) {
        return {uri: image};
    }
    render() {
        return (
            <View style={HomeScreenStyle.cardItem}>
                <TouchableOpacity
                 onPress = {this.props.action}
                >
                    <Image
                        source = {this.setImage(this.props.image)}
                        style = {HomeScreenStyle.cardItem_image}
                    >
                    </Image>
                    <Text style={HomeScreenStyle.cardItem_text}>{this.props.title}</Text>
                    <View style={HomeScreenStyle.cardItem_bottomView} >
                        <Text style= {{fontSize: 12, color: 'red', fontWeight: 'bold'}}>{this.props.price}đ</Text>
                    </View>
                </TouchableOpacity>
                
            </View>
        )
    }
}