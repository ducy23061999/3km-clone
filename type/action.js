import * as Type from './type'

export const setProducts = (products) => {
    return {type: Type.SET_LIST_PRODUCT, payload: products};
}
export const setCategory = (category) => {
    return {type: Type.SET_NEW_CATEGORY, payload: category}
}
