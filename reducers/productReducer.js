import * as Type from '../type/type'

const initState = []
const productReducer = (state = initState, action)  => {
    switch(action.type) {
        case Type.SET_LIST_PRODUCT: {
            return action.payload
        }
        default:
            return initState
    }
}
export default productReducer