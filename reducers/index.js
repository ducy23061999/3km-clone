import {combineReducers} from 'redux'
import categoryReducer from './categoryReducer'
import productReducer from './productReducer'

export default rootReducer = combineReducers({
    category: categoryReducer,
    products: productReducer
})

