import * as Type from '../type/type'

const initState = {}
const categoryReducer = (state = initState, action)  => {
    switch(action.type) {
        case Type.SET_NEW_CATEGORY: {
            return action.payload
        }
        default:
            return initState
    }
}
export default categoryReducer