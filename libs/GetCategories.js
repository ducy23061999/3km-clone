const axios = require('axios').default;

import Category from '../model/Category';

const api = require('../configs/api.json');
const getCategoriesApi = api.getCategories;

function getListCategory() {
    return axios.get(getCategoriesApi)
}

export async function getAllCategories() {
   try {
     const response = await getListCategory();
     const {data} = response.data;
     const listCategory = data.map((category) => {
           return new Category(category)
     });
     return listCategory
   } catch (e) {
     console.error(e)
   }
    
}