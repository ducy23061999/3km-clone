const axios = require('axios').default;

import Product from '../model/Product';

const api = require('../configs/api.json');
const getProductApi = api.getProducts;

export default class GetProducts {

    static getListProducts() {
        return new Promise((resolve, reject) => {
            
            let productList = [];
            axios.get(getProductApi)
            .then((response) => {
                let products = response.data.data;
                
                products.forEach(product => {
                    let wrapProduct = new Product(product);
                    productList.push(wrapProduct);
                });

                resolve(productList);
      
            })
            .catch((error) => {
                reject(error)
            })
        })
        
    }
}

