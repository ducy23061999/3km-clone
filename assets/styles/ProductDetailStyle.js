import {StyleSheet, Dimensions} from 'react-native'

let screenWidth = Dimensions.get('window').width;
export const ProductDetailStyle = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#f2f2f0'
    },
    productInfo: {
        width: screenWidth,
        height: 335,
        backgroundColor: 'white'
        
    },
    productInfoImage: {
        width: screenWidth,
        height: 235
    },
    productInfoTextBlock: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        marginLeft: 10,
        color: 'white'
    },
    productInfoTextTintColor: {
        color: 'red'
    },
    productInfoDisableText: {
        color: 'grey'
    },
    productInfoMarginItem: {
        marginTop: 10,
        marginBottom: 10
    },
    productStatus: {
        height: 75,
        marginTop: 10,
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: 'flex-start',
    },
    textMargin: {
        marginTop: 10,
        marginBottom: 10
    },
    tag: {
        maxHeight: 200,
        backgroundColor: 'white',
        marginTop: 10,
        width: screenWidth,
    },
    tagContent: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
        paddingTop: 20
        
    },
    tagView: {
        display: 'flex',
        backgroundColor: 'white',
        height: 40,
        borderColor: 'black',
        borderRadius: 10,
        borderWidth: 1,
        maxWidth: 100,
        minWidth: 70,
        justifyContent: "center",
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    textOfTag: {
        fontSize: 14,
        marginLeft: 10,
        marginRight: 10
    },
    avata: {
        width: 75,
        height: 75,
        borderRadius: 74/2
    },
    textBlock: {
        fontSize: 14,
        marginLeft: 20
    },
    topView: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        marginTop: 10
    },
    reviewBlockParent: {
        height: 80,
        backgroundColor: 'white'
    },
    reviewBlockChild: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        height: 80,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        borderTopWidth: 1,
        borderTopColor: '#b6b9bf'
    },
    reviewBlock: {
        height: 75,
        width: (screenWidth - 20) / 3,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    line: {
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderRightColor: '#b6b9bf',
        borderLeftColor: '#b6b9bf',
        height: 50,
    },
    noneLine: {
        height: 50,
    },
    button: {
        backgroundColor: '#d812db',
        height: 40,
    }

});