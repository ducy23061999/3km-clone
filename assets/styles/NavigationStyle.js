import {StyleSheet, Dimensions, Platform} from 'react-native'
import { Left } from 'native-base';

let screenWidth = Dimensions.get('window').width;
let navigationHomeHeight = 100;
let heightStatusBar = Platform.OS == 'ios' ? 28 : 0;

export const HomeNavigationStyle = StyleSheet.create({
    container: {
        width: screenWidth,
        height: navigationHomeHeight,
        marginTop: heightStatusBar,
    },
    topView: {
        width: screenWidth,
        height: navigationHomeHeight / 2,
        backgroundColor: '#ff4d82',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text : {
        flex: 1.5,
        color: 'white',
        fontWeight: 'bold',
        marginLeft: 15
    },
    searchView: {
        flex: 6
    },
    searchInputContainer: {
        backgroundColor: 'white',
        height: navigationHomeHeight / 2 - 40,
        marginTop: 2,
        
    },
    inputStyle: {
        backgroundColor: '#ff4d82',
        height: navigationHomeHeight / 2 + 2
    },
    textSearchInput: {
        fontSize: 14
    },
    commingSoon: {
        position: 'relative',
        left: 10,
        top: 15

    },
    categoriesButton: {
        height: 60,
        marginTop: 4
        
    } ,
    button: {
        backgroundColor: '#ff4d82'
    }
});

