import {StyleSheet, Dimensions} from 'react-native'

let screenWidth = Dimensions.get('window').width;
let itemWidth = screenWidth / 2 - 20

const HomeScreenStyle = StyleSheet.create({
    scrollview: {
        backgroundColor: '#f2f2f0'
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop: 20,
        justifyContent: 'space-around',
        backgroundColor: '#f2f2f0',
        flexGrow: 1,
    },
    cardItem: {
        width: itemWidth,
        backgroundColor: 'white',
        borderRadius: 4,
        marginBottom: 20, 
        flex: 0/5,
        margin: 5
    },
    flatListStyle: {
        marginLeft: 10

    },
    cardItem_image: {
        width: itemWidth,
        height: itemWidth - 20,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4
    },
    cardItem_text: {
        marginTop: 5,
        paddingLeft: 8,
        fontSize: 12,
    },
    cardItem_bottomView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        paddingLeft: 8,
        paddingRight: 8,
        marginBottom: 5
    },
    footer: {
        width: '100%',
        height: 80,
        
    },
    bannerContainer: {
        width: screenWidth - 20,
        height: 300,
        backgroundColor: 'blue'
    }
})

export default HomeScreenStyle;