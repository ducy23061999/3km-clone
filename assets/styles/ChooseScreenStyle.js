import {StyleSheet, Dimensions} from 'react-native'

const {width, height} = Dimensions.get('window');
export default ChooseScreenStyle = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: width,
        height: height,
        backgroundColor: '#f2f2f0'

    },
    marginView: {
        marginLeft: 16,
        marginRight: 16,
        marginTop: 100,
    },
    text: {
        fontSize: 18,
        fontStyle: 'italic'
    },
    marginElement: {
        marginTop: 5,
        marginEnd: 5
    },
    fixButton: {
        position: 'relative',
        bottom: '0%',
        right: '0%'
    },
    dropDownContainer: {
        color: 'black'
    },
    picker: {
        backgroundColor: '#ff4d82'
    },
    item: {
        color: 'white'
    }
    
})