import {createStore, applyMiddleware} from 'redux'
import reducers from '../reducers'
import rootSaga from '../saga'
import createSagaMiddleware from 'redux-saga'


const sagaMidleware = createSagaMiddleware();

const store = createStore(
    reducers,
    {},
    applyMiddleware(sagaMidleware)
)

sagaMidleware.run(rootSaga);

export default store;