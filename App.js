import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {AppStyle} from './assets/styles/AppStyle';
import {createAppContainer, createSwitchNavigator} from  'react-navigation';
import BottomTabNavigator from './modules/views/BottomTabNavigator';
import {Provider} from 'react-redux'
import store from './store'

const App =  createAppContainer(
  createSwitchNavigator({
    Main: BottomTabNavigator
  })
);

export default () => {
  return (
    <Provider store = {store}>
        <App/>
    </Provider>
  )
};



