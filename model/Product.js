

export default class Product {

    constructor(props) {
        try {
            this.id = props._id
            this.name = props.name;
            this.description = props.description;
            this.status = props.status;
            this.price = props.price;
            this.min_offer = props.min_offer;
            this.condition = props.condition;
            this.isActive = props.is_active;
            this.like = props.like;
            this.postedOn = props.posted_on;
            this.image = props.images[0].medium.image_url;
            this.categoryName = props.category.name;
            this.categoryId = props.category._id;
        } catch (e) {
            this.image = "https://cafebiz.cafebizcdn.vn/thumb_w/600/2019/photo1552354590566-1552354590734-crop-15523545973941877966906.jpg"
        }
        
    } 
     
    
    
    
}