export default class Category {
    constructor(props) {
        this.id = props._id;
        this.name = props.name;
        this.slug = props.slug;
    }
}